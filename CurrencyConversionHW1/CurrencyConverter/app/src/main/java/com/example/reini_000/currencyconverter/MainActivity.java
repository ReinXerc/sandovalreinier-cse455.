package com.example.reini_000.currencyconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    private EditText editText01;
    private TextView textView01;
    private String usd;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editText01 = findViewById(R.id.EditText1);
        Button bnt01 = findViewById(R.id.bnt);
        textView01 = findViewById(R.id.Yen);

        bnt01.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View CYO) {
                double dInput;
                double dResult;
                usd = editText01.getText().toString();
                if(usd.equals("")) {
                    textView01.setText("This field cannot be blank!");
                    editText01.setText("");
                } else {
                    dInput = Double.parseDouble(usd);
                    dResult = dInput * 112.57;

                    textView01.setText("$" + usd + " = " + "¥"+String.format("%.2f",
                            dResult));
                    editText01.setText("");


                }
        };
    });
}}
